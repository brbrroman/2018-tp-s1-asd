/*
 * Language: C++17
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 2.2
 *                              Порядок обхода
 * Дано число N < 10^6 и последовательность целых чисел из [-2^31..2^31] длиной
 * N.
 * Требуется построить бинарное дерево, заданное наивным порядком вставки.
 * Т.е., при добавлении очередного числа K в дерево с корнем root, если root→Key
 * ≤ K, то узел K добавляется в правое поддерево root; иначе в левое поддерево
 * root.
 * Рекурсия запрещена.
 *
 * Выведите элементы в порядке pre-order (сверху вниз).
 *
 * Пример ввода       | Пример вывода
 * ===================|=================
 *   3                |   2   1   3
 *   2   1   3        |
 * -------------------+-----------------
 *   3                |   1   2   3
 *   1   2   3        |
 * -------------------+-----------------
 *   3                |   3   1   2
 *   3   1   2        |
 * -------------------+-----------------
 *   4                |   3   1   2   4
 *   3   1   4   2    |
 * ___________________|_________________
 */

#include <iostream>
#include <stack>

template <typename T>
class BinaryTree {
 public:
  /* Конструктор по умолчанию */
  BinaryTree() = default;
  /* Конструктор копирования */
  BinaryTree(const BinaryTree &other) = delete;
  /* Оператор присваивания копированием */
  BinaryTree &operator=(const BinaryTree &other) = delete;
  /* Конструктор перемещения */
  BinaryTree(BinaryTree &&other) = delete;
  /* Оператор присваивания перемещением */
  BinaryTree &operator=(BinaryTree &&other) = delete;
  /* Деструктор */
  ~BinaryTree();
  void push(const T &value);
  void print_tree_pre_order() const;

 private:
  /* Структура уза */
  struct Node {
    explicit Node(const T &value) : value(value) {}
    T value;
    Node *left = nullptr;
    Node *right = nullptr;
  };
  /* Корень дерева */
  Node *root = nullptr;
};

template <typename T>
void BinaryTree<T>::push(const T &value) {
  if (root == nullptr) {
    root = new Node(value);
    return;
  }
  Node *current_node = root;
  while (true) {
    if (value < current_node->value) {
      if (current_node->left == nullptr) {
        current_node->left = new Node(value);
        break;
      }
      current_node = current_node->left;
    } else {
      if (current_node->right == nullptr) {
        current_node->right = new Node(value);
        break;
      }
      current_node = current_node->right;
    }
  }
}

template <typename T>
void BinaryTree<T>::print_tree_pre_order() const {
  std::stack<Node *> stack;
  Node *root = this->root;
  while (true) {
    while (root != nullptr) {
      std::cout << root->value << ' ';
      stack.push(root);
      root = root->left;
    }
    if (stack.empty()) {
      break;
    }
    root = stack.top();
    stack.pop();
    root = root->right;
  }
  std::cout << std::endl;
}

template <typename T>
BinaryTree<T>::~BinaryTree() {
  if (root == nullptr) return;
  std::stack<Node *> stack_for_delete;
  stack_for_delete.push(root);
  while (!stack_for_delete.empty()) {
    Node *node = stack_for_delete.top();
    stack_for_delete.pop();
    if (node->left != nullptr) {
      stack_for_delete.push(node->left);
    }
    if (node->right != nullptr) {
      stack_for_delete.push(node->right);
    }
    delete node;
  }
}

/* Main body */
int main(int argc, char *argv[]) {
  size_t size = 0;
  std::cin >> size;
  BinaryTree<int> tree;
  int value;
  for (size_t i = 0; i < size; ++i) {
    std::cin >> value;
    tree.push(value);
  }
  tree.print_tree_pre_order();
  return 0;
}
