/*
 * Language: C++17
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 4.2
 *                         Использование АВЛ-дерева
 *                          Порядковые статистики
 * Дано число N и N строк. Каждая строка содержит команду добавления или
 * удаления натуральных чисел, а также запрос на получение k-ой порядковой
 * статистики. Команда добавления числа A задается положительным числом A,
 * команда удаления числа A задается отрицательным числом “-A”. Запрос на
 * получение k-ой порядковой статистики задается числом k. Требуемая скорость
 * выполнения запроса - O(log n).
 *
 * Пример ввода   | Пример вывода
 * =================|=================
 *    5             | 40
 *   40 0           | 40
 *   10 1           | 10
 *    4 1           | 4
 *  -10 0           | 50
 *   50 2           |
 * _________________|_________________
 */

#include <iostream>
#include <stack>

template <typename T>
class AVL {
 public:
  AVL() = default;
  AVL(const AVL &other) = delete;
  AVL &operator=(const AVL &other) = delete;
  AVL(AVL &&other) = delete;
  AVL &operator=(AVL &&other) = delete;
  ~AVL();

  void add(const T &value) { root = add(root, value); };
  void remove(const T &value) { root = remove(root, value); };
  const T& stat(size_t index) const;

 private:
  struct Node {
    T value;
    size_t height = 1;
    size_t size = 1;
    Node *left = nullptr;
    Node *right = nullptr;

    explicit Node(const T &value) : value(value){};
  };

  size_t height(Node *root) const { return root ? root->height : 0; };
  size_t size(Node *root) const { return root ? root->size : 0; };

  Node *add(Node *root, const T &value);
  Node *big_rotation(Node *tree);
  Node *remove(Node *root, const T &value);
  Node *find_remove_min(Node *root, Node *&parent);
  Node *find_remove_max(Node *root, Node *&parent);
  void recalc_height(Node *root) {
    root->height = std::max(height(root->right), height(root->left)) + 1;
  };
  void recalc_size(Node *root) {
    root->size = 1 + size(root->left) + size(root->right);
  };
  Node *rotation(Node *q, bool right);
  Node *rotateleft(Node *root) { return rotation(root, false); };
  Node *rotateright(Node *root) { return rotation(root, true); };
  int bfactor(Node *root) {
    return int(height(root->right) - height(root->left));
  };
  Node *root = nullptr;
};

template <class T>
AVL<T>::~AVL() {
  if (root == nullptr) return;
  std::stack<Node *> stack_for_delete;
  stack_for_delete.push(root);
  while (!stack_for_delete.empty()) {
    Node *node = stack_for_delete.top();
    stack_for_delete.pop();
    if (node->left) {
      stack_for_delete.push(node->left);
    }
    if (node->right) {
      stack_for_delete.push(node->right);
    }
    delete node;
  }
}

template <class T>
T AVL<T>::stat(size_t index) const {
  index %= size(root);
  Node *current_node = root;
  while (true) {
    size_t left_size = size(current_node->left);
    if (left_size < index) {
      current_node = current_node->right;
      index -= left_size + 1;
    } else if (index < left_size) {
      current_node = current_node->left;
    } else {
      break;
    }
  }
  return current_node->value;
}

template <class T>
typename AVL<T>::Node *AVL<T>::find_remove_min(AVL<T>::Node *root,
                                               AVL<T>::Node *&parent) {
  if (root->left) {
    root->left = find_remove_min(root->left, parent);
    return big_rotation(root);
  } else {
    parent = root;
    return root->right;
  }
}

template <class T>
typename AVL<T>::Node *AVL<T>::find_remove_max(AVL<T>::Node *root,
                                               AVL<T>::Node *&parent) {
  if (root->right) {
    root->right = find_remove_max(root->right, parent);
    return big_rotation(root);
  } else {
    parent = root;
    return root->left;
  }
}

template <class T>
typename AVL<T>::Node *AVL<T>::rotation(AVL<T>::Node *root, bool right) {
  Node *new_root;
  if (!right) {
    new_root = root->right;
    root->right = new_root->left;
    new_root->left = root;
  } else {
    new_root = root->left;
    root->left = new_root->right;
    new_root->right = root;
  }
  recalc_height(root);
  recalc_height(new_root);
  recalc_size(root);
  recalc_size(new_root);
  return new_root;
}

template <class T>
typename AVL<T>::Node *AVL<T>::big_rotation(AVL<T>::Node *root) {
  recalc_size(root);
  recalc_height(root);

  if (bfactor(root) == 2) {
    if (bfactor(root->right) < 0) {
      root->right = rotateright(root->right);
    }
    return rotateleft(root);
  }
  if (bfactor(root) == -2) {
    if (bfactor(root->left) > 0) {
      root->left = rotateleft(root->left);
    }
    return rotateright(root);
  }
  return root;
}

template <class T>
typename AVL<T>::Node *AVL<T>::add(Node *root, const T &value) {
  if (root) {
    if (value < root->value) {
      root->left = add(root->left, value);
    } else {
      root->right = add(root->right, value);
    }
    return big_rotation(root);
  } else {
    return new Node(value);
  }
}

template <class T>
typename AVL<T>::Node *AVL<T>::remove(Node *root, const T &value) {
  if (root) {
    if (value < root->value) {
      root->left = remove(root->left, value);
    } else if (root->value < value) {
      root->right = remove(root->right, value);
    } else {
      Node *left = root->left;
      Node *right = root->right;
      delete root;
      if ((right) and (left)) {
        if (height(left) < height(right)) {
          Node *min = nullptr;
          Node *min_right = find_remove_min(right, min);
          min->right = min_right;
          min->left = left;
          return big_rotation(min);
        } else {
          Node *max = nullptr;
          Node *max_left = find_remove_max(left, max);
          max->left = max_left;
          max->right = right;
          return big_rotation(max);
        }
      } else {
        return right ? right : left;
      }
    }
    return big_rotation(root);
  } else {
    return nullptr;
  }
}

/* Main body */
int main(int argc, char *argv[]) {
  size_t size;
  std::cin >> size;
  AVL<int> tree;
  int value;
  size_t index;
  for (size_t i = 0; i < size; ++i) {
    std::cin >> value >> index;
    if (value < 0) {
      tree.remove(-value);
    } else {
      tree.add(value);
    }
    std::cout << tree.stat(index) << std::endl;
  }
  return 0;
}
