/*
 * Language: C++14
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 1.2
 *                                Хэш-таблица
 * Реализуйте структуру данных типа “множество строк” на основе динамической
 * хеш-таблицы с открытой адресацией. Хранимые строки непустые и состоят из
 * строчных латинских букв.
 * Хеш-функция строки должна быть реализована с помощью вычисления значения
 * многочлена методом Горнера.
 * Начальный размер таблицы должен быть равным 8-ми. Перехеширование выполняйте
 * при добавлении элементов в случае, когда коэффициент заполнения таблицы
 * достигает 3/4.
 * Структура данных должна поддерживать операции добавления строки в множество,
 * удаления строки из множества и проверки принадлежности данной строки
 * множеству.
 *
 * Для разрешения коллизий используйте двойное хеширование.
 *
 * Формат входных данных
 * Каждая строка входных данных задает одну операцию над множеством. Запись
 * операции состоит из типа операции и следующей за ним через пробел строки, над
 * которой проводится операция.
 * Тип операции  – один из трех символов:
 *  +  означает добавление данной строки в множество;
 *  -  означает удаление  строки из множества;
 *  ?  означает проверку принадлежности данной строки множеству.
 * При добавлении элемента в множество НЕ ГАРАНТИРУЕТСЯ, что он отсутствует в
 * этом множестве. При удалении элемента из множества НЕ ГАРАНТИРУЕТСЯ, что он
 * присутствует в этом множестве.
 *
 * Формат выходных данных
 * Программа должна вывести для каждой операции одну из двух строк OK или FAIL,
 в зависимости от того, встречается ли данное слово в нашем множестве.

 *
 * Пример ввода | Пример вывода
 * =============|=================
 *  + hello     | OK
 *  + bye       | OK
 *  ? bye       | OK
 *  + bye       | FAIL
 *  - bye       | OK
 *  ? bye       | FAIL
 *  ? hello     | OK
 * _____________|_________________
 */

#include <cmath>
#include <iostream>
#include <string>

template <typename T>
class HashTable {
 public:
  HashTable(const T& empty_marker, const T& del_marker);
  ~HashTable();
  HashTable(const HashTable& other) = delete;
  HashTable& operator=(const HashTable& other) = delete;
  HashTable(HashTable&& other) = delete;
  HashTable& operator=(HashTable&& other) = delete;

  bool has(const T& key) const;
  bool add(const T& key);
  bool remove(const T& key);

 private:
  const size_t HASH_CONST = 127;
  const size_t ANOTHER_HASH_CONST = 137;
  const size_t DEFAULT_SIZE = 2;

  const float ALPHA_MAX = 0.7;

  T EMPTY_MARKER;
  T DEL_MARKER;

  size_t size;
  size_t elements_count = 0;
  T* table;

  double alpha() const { return (float)elements_count / (float)size; };
  size_t hash(const T& key, const size_t& hash_var) const;
  size_t find_suiitable_index(const T& key, const size_t& hash,
                              const size_t& hash2) const;
  void rehash();
  size_t new_hash(const size_t& hash, const size_t& i,
                  const size_t& hash2) const;
};

template <typename T>
HashTable<T>::HashTable(const T& empty_marker, const T& del_marker)
    : EMPTY_MARKER(empty_marker), DEL_MARKER(del_marker), size(DEFAULT_SIZE) {
  table = new T[size];
  std::fill(table, table + size, EMPTY_MARKER);
}

template <typename T>
HashTable<T>::~HashTable() {
  delete[] table;
}

template <typename T>
size_t HashTable<T>::find_suiitable_index(const T& key, const size_t& hash,
                                          const size_t& hash2) const {
  if (table[hash] == key) {
    return hash;
  }
  size_t i = 0;
  size_t index_for_check = 0;
  do {
    index_for_check = new_hash(hash, ++i, hash2);
  } while ((index_for_check != hash) &&
           (table[index_for_check] != EMPTY_MARKER) &&
           (table[index_for_check] != key));
  return index_for_check;
}

template <typename T>
bool HashTable<T>::has(const T& key) const {
  size_t index_for_check = find_suiitable_index(
      key, this->hash(key, HASH_CONST), this->hash(key, ANOTHER_HASH_CONST));
  return (table[index_for_check] == key);
}

template <typename T>
bool HashTable<T>::add(const T& key) {
  if (ALPHA_MAX < alpha()) {
    rehash();
  }
  size_t hash = this->hash(key, HASH_CONST);
  size_t hash2 = this->hash(key, ANOTHER_HASH_CONST);
  size_t index_for_check = 0;
  size_t index_for_replace = size;
  if (table[hash] == key) {
    index_for_check = hash;
  } else {
    size_t i = 0;
    do {
      index_for_check = new_hash(hash, ++i, hash2);
      if ((table[index_for_check] == DEL_MARKER) and
          (index_for_replace == size)) {
        index_for_replace = index_for_check;
      }
    } while ((index_for_check != hash) &&
             (table[index_for_check] != EMPTY_MARKER) &&
             (table[index_for_check] != key));
  }

  if (table[index_for_check] == key) {
    return false;
  } else {
    ++elements_count;
    if (index_for_replace == size) {
      table[index_for_check] = key;
    } else {
      table[index_for_replace] = key;
    }
    return true;
  }
}

template <typename T>
bool HashTable<T>::remove(const T& key) {
  size_t index_for_check = find_suiitable_index(
      key, this->hash(key, HASH_CONST), this->hash(key, ANOTHER_HASH_CONST));
  if (table[index_for_check] == key) {
    table[index_for_check] = DEL_MARKER;
    --elements_count;
    return true;
  } else {
    return false;
  }
}

template <typename T>
void HashTable<T>::rehash() {
  /* Т.к. перехеширрование производится на массиве table, то
   * размер массива необходимо увеличить до вызова функции add(), которая
   * вызывает hash(), которая зависит от size
   */
  size *= 2;
  T* temp_table = table;
  table = new T[size];
  std::fill(table, table + size, EMPTY_MARKER);
  elements_count = 0;
  for (size_t i = 0; i < size / 2; ++i) {
    if (temp_table[i] != EMPTY_MARKER && temp_table[i] != DEL_MARKER) {
      add(temp_table[i]);
    }
  }
  delete[] temp_table;
}

template <typename T>
size_t HashTable<T>::hash(const T& key, const size_t& hash_var) const {
  size_t result = 0;
  for (size_t i = 0; i < key.size(); ++i) {
    result = (result * hash_var + key[i] + 1) % size;
  }
  return (result);
}

template <typename T>
size_t HashTable<T>::new_hash(const size_t& hash, const size_t& i,
                              const size_t& hash2) const {
  return (hash + hash2 * i) % size;
}

/* Main body */
int main(int argc, char* argv[]) {
  HashTable<std::string> myHashTable("", "%removed%");
  // HashTable<int> my_int_hash(-1, -2);
  std::string payload;
  bool finish = false;
  while (!finish and std::getline(std::cin, payload)) {
    char command = payload[0];
    payload.erase(0, 2);
    switch (command) {
      case '+':
        std::cout << (myHashTable.add(payload) ? "OK" : "FAIL") << std::endl;
        break;
      case '-':
        std::cout << (myHashTable.remove(payload) ? "OK" : "FAIL") << std::endl;
        break;
      case '?':
        std::cout << (myHashTable.has(payload) ? "OK" : "FAIL") << std::endl;
        break;
      default:
        finish = true;
        break;
    }
  }
  return 0;
}
