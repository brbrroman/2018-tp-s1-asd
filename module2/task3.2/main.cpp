/*
 * Language: C++17
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 3.2
 *                             Декартово дерево
 * Дано число N < 10^6 и последовательность пар целых чисел из [-2^31..2^31]
 * длиной N.
 * Построить декартово дерево из N узлов, характеризующихся парами чисел {Xi,
 * Yi}.
 * Каждая пара чисел {Xi, Yi} определяет ключ Xi и приоритет Yi в декартовом
 * дереве.
 * Добавление узла в декартово дерево выполняйте второй версией алгоритма,
 * рассказанного на лекции:
 * При добавлении узла (x, y) выполняйте спуск по ключу до узла P с меньшим
 * приоритетом. Затем разбейте найденное поддерево по ключу x так, чтобы в
 * первом поддереве все ключи меньше x, а во втором больше или равны x.
 * Получившиеся два дерева сделайте дочерними для нового узла (x, y). Новый узел
 * вставьте на место узла P.
 *
 * Построить также наивное дерево поиска по ключам Xi методом из задачи 2.
 *
 * Вычислить количество узлов в самом широком слое декартового дерева и
 * количество узлов в самом широком слое наивного дерева поиска. Вывести их
 * разницу. Разница может быть отрицательна.
 *
 * Пример ввода     | Пример вывода
 * =================|=================
 *  10              |   1
 *   5  11          |
 *  18   8          |
 *  25   7          |
 *  50  12          |
 *  30  30          |
 *  15  15          |
 *  20  10          |
 *  22   5          |
 *  40  20          |
 *  45   9          |
 * -----------------+-----------------
 *  10              |   1
 *  38  19
 *  37   5
 *  47  15
 *  35   0
 *  12   3
 *   0  42
 *  31  37
 *  21  45
 *  30  26
 *  41   6
 * _________________|_________________
 */

#include <iomanip>
#include <iostream>
#include <queue>
#include <stack>
#include <string>

template <typename T>
class BinaryTree {
 public:
  BinaryTree() = default;
  BinaryTree(const BinaryTree &other) = delete;
  BinaryTree &operator=(const BinaryTree &other) = delete;
  BinaryTree(BinaryTree &&other) = delete;
  BinaryTree &operator=(BinaryTree &&other) = delete;
  ~BinaryTree();
  void push(const T &value);
  size_t get_max_width() const;

 private:
  struct Node {
    explicit Node(const T &value) : value(value) {}
    T value;
    Node *left = nullptr;
    Node *right = nullptr;
  };
  Node *root = nullptr;
};

template <typename T>
void BinaryTree<T>::push(const T &value) {
  if (root == nullptr) {
    root = new Node(value);
    return;
  }
  Node *current_node = root;
  while (true) {
    if (value < current_node->value) {
      if (current_node->left == nullptr) {
        current_node->left = new Node(value);
        break;
      }
      current_node = current_node->left;
    } else {
      if (current_node->right == nullptr) {
        current_node->right = new Node(value);
        break;
      }
      current_node = current_node->right;
    }
  }
}

template <typename T>
size_t BinaryTree<T>::get_max_width() const {
  std::queue<Node *> queue;
  size_t max_width = 0;
  size_t current_width = 0;
  if (this->root == nullptr) return current_width;
  queue.push(this->root);
  queue.push(nullptr);
  while (true) {
    if (queue.front() == nullptr) {
      queue.pop();
      if (max_width < current_width) {
        max_width = current_width;
      }
      current_width = 0;
      if (queue.empty()) return max_width;
      queue.push(nullptr);
    }
    Node *node = queue.front();
    current_width++;
    queue.pop();
    if (node->left) {
      queue.push(node->left);
    }
    if (node->right) {
      queue.push(node->right);
    }
  }
}

template <typename T>
BinaryTree<T>::~BinaryTree() {
  if (root == nullptr) return;
  std::stack<Node *> stack_for_delete;
  stack_for_delete.push(root);
  while (!stack_for_delete.empty()) {
    Node *node = stack_for_delete.top();
    stack_for_delete.pop();
    if (node->left != nullptr) {
      stack_for_delete.push(node->left);
    }
    if (node->right != nullptr) {
      stack_for_delete.push(node->right);
    }
    delete node;
  }
}

template <typename T1, typename T2>
class Treap {
 public:
  Treap() = default;
  Treap(const Treap &other) = delete;
  Treap &operator=(const Treap &other) = delete;
  Treap(Treap &&other) = delete;
  Treap &operator=(Treap &&other) = delete;
  ~Treap();
  void push(const T1 &value, const T2 &priority);
  size_t get_max_width() const;

 private:
  struct Node {
    explicit Node(const T1 &value, const T2 priority)
        : value(value), priority(priority) {}
    T1 value;
    T2 priority;
    Node *left = nullptr;
    Node *right = nullptr;
  };
  Node *root = nullptr;
  void split(Node *current_node, const T1 value, Node *&left, Node *&right);
  Node *merge(Node *left, Node *right);
  void insert(Node *&t, Node *it);
};

template <typename T1, typename T2>
void Treap<T1, T2>::split(Node *current_node, const T1 value, Node *&left,
                          Node *&right) {
  /*
   * warning: Assignment of function parameter has no effect outside the
   * function. Did you forget dereferencing it? NEED *& for left and right
   */
  left = nullptr;
  right = nullptr;
  if (current_node) {
    if (value < current_node->value) {
      split(current_node->left, value, left, current_node->left);
      right = current_node;
    } else {
      split(current_node->right, value, current_node->right, right);
      left = current_node;
    }
  }
}

template <typename T1, typename T2>
void Treap<T1, T2>::push(const T1 &value, const T2 &priority) {
  insert(root, new Node(value, priority));
}

template <typename T1, typename T2>
void Treap<T1, T2>::insert(Node *&root, Node *node_to_insert) {
  if (!root) {
    root = node_to_insert;
  } else if (node_to_insert->priority > root->priority) {
    split(root, node_to_insert->value, node_to_insert->left,
          node_to_insert->right),
        root = node_to_insert;
  } else {
    insert(node_to_insert->value < root->value ? root->left : root->right,
           node_to_insert);
  }
}

template <typename T1, typename T2>
typename Treap<T1, T2>::Node *Treap<T1, T2>::merge(Node *left, Node *right) {
  if (left and right) {
    if (right->priority < left->priority) {
      left->right = merge(left->right, right);
      return left;
    } else {
      right->left = merge(left, right->left);
      return right;
    }
  } else {
    return left ? left : right;
  }
}

template <typename T1, typename T2>
size_t Treap<T1, T2>::get_max_width() const {
  std::queue<Node *> queue;
  size_t max_width = 0;
  size_t current_width = 0;
  if (this->root == nullptr) return current_width;
  queue.push(this->root);
  queue.push(nullptr);
  while (true) {
    if (queue.front() == nullptr) {
      queue.pop();
      if (max_width < current_width) {
        max_width = current_width;
      }
      current_width = 0;
      if (queue.empty()) return max_width;
      queue.push(nullptr);
    }
    Node *node = queue.front();
    current_width++;
    queue.pop();
    if (node->left != nullptr) {
      queue.push(node->left);
    }
    if (node->right != nullptr) {
      queue.push(node->right);
    }
  }
}

template <typename T1, typename T2>
Treap<T1, T2>::~Treap() {
  if (root == nullptr) return;
  std::stack<Node *> stack_for_delete;
  stack_for_delete.push(root);
  while (!stack_for_delete.empty()) {
    Node *node = stack_for_delete.top();
    stack_for_delete.pop();
    if (node->left != nullptr) {
      stack_for_delete.push(node->left);
    }
    if (node->right != nullptr) {
      stack_for_delete.push(node->right);
    }
    delete node;
  }
}

/* Main body */
int main(int argc, char *argv[]) {
  {
    size_t size = 0;
    std::cin >> size;
    BinaryTree<int> binary_tree;
    {
      Treap<int, int> treap;
      int key, priority;
      for (size_t i = 0; i < size; ++i) {
        std::cin >> key >> priority;
        binary_tree.push(key);
        treap.push(key, priority);
      }
      int Tw = (int)treap.get_max_width();
      int Bw = (int)binary_tree.get_max_width();
      std::cout << Tw - Bw << std::endl;
    }
  }
  return 0;
}
