/*
 * Language: C++14
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 2.4
 * Дан отсортированный массив различных целых чисел A[0..n-1] и массив целых
 * чисел B[0..m-1]. Для каждого элемента массива B[i] найдите минимальный индекс
 * элемента массива A[k], ближайшего по значению к B[i]. Время работы поиска для
 * каждого элемента B[i]: O(log(k)).
 * n ≤ 110_000, m ≤ 1_000.
 *
 * Пример ввода       | Пример вывода
 * ===================|=================
 *   3                |   0   0   2
 *  10  20  30        |
 *   3                |
 *   9  15  35        |
 * -------------------+-----------------
 *   3                |   0   0   0   2
 *  10  20  30        |
 *   4                |
 *   8   9  10  32    |
 * ___________________|_________________
 */

#include <iostream>

template <typename T>
size_t binarySearch(const T* array, size_t left, size_t size, const T& elem) {
  size_t right = size - 1;
  while (left + 1 < right) {
    size_t middle = (right + left) / 2;
    if (elem < array[middle]) {
      right = middle;
    } else {
      left = middle;
    }
  }
  if (abs(array[left] - elem) <= abs(array[right] - elem)) {
    return left;
  } else {
    return right;
  }
}

template <typename T>
size_t nearest(const T* array, const size_t size, const T& elem) {
  size_t left_index = 0;
  size_t right_index = 1;
  if (elem <= array[left_index]) return left_index;
  while ((right_index < size) and (elem > array[right_index])) {
    left_index = right_index;
    right_index <<= 1;
  }
  right_index = std::min(size, right_index + 1);
  return binarySearch(array, left_index, right_index, elem);
}

int* inputArray(size_t const count) {
  int* array = new int[count];
  for (size_t i = 0; i < count; ++i) {
    std::cin >> array[i];
  }
  return array;
}

template <typename T>
void printArray(T* const from_index, T* const to_index) {
  for (T* iter = from_index; iter < to_index; ++iter) {
    std::cout << *iter << ' ';
  }
  std::cout << std::endl;
}

template <typename T>
void nearest_array(const T* first_array, const size_t first_size,
                   const T* second_array, const size_t second_size,
                   size_t* array) {
  for (size_t i = 0; i < second_size; ++i) {
    array[i] = nearest(first_array, first_size, second_array[i]);
  };
}

/* Main body */
int main(int argc, char* argv[]) {
  size_t first_size, second_size;

  std::cin >> first_size;
  int* first_array = inputArray(first_size);

  std::cin >> second_size;
  int* second_array = inputArray(second_size);

  size_t* result_array = new size_t[second_size];
  nearest_array(first_array, first_size, second_array, second_size,
                result_array);
  printArray(result_array, result_array + second_size);

  delete[] result_array;
  result_array = nullptr;
  delete[] first_array;
  first_array = nullptr;
  delete[] second_array;
  second_array = nullptr;
  return 0;
}
