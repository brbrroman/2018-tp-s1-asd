/*
 * Language: C++14
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 3.1
 *
 * Реализовать очередь с динамическим зацикленным буфером.
 *
 * Обрабатывать команды push back и pop front.
 *
 * Формат ввода
 * В первой строке количество команд n. n ≤ 1000000.
 *
 * Каждая команда задаётся как 2 целых числа: a b.
 *
 * a = 2 - pop front
 * a = 3 - push back
 *
 * Если дана команда pop front, то число b - ожидаемое значение. Если
 * команда pop front вызвана для пустой структуры данных, то
 * ожидается “-1”.
 *
 * Формат вывода
 * Требуется напечатать YES - если все ожидаемые значения совпали.
 * Иначе, если хотя бы одно ожидание не оправдалось, то напечатать NO.
 *
 * Пример 1
 * Ввод 	Вывод
 * 3		YES
 * 3 44
 * 3 50
 * 2 44
 *
 */

#include <assert.h>
#include <cstring>
#include <iostream>

template <typename T>
class Queue {
 public:
  Queue();
  ~Queue();
  /* Конструктор копирования */
  Queue(const Queue& other) = delete;
  /* Оператор присваивания копированием */
  Queue& operator=(const Queue& other) = delete;
  /* Конструктор перемещения */
  Queue(Queue&& other) = delete;
  /* Оператор присваивания перемещением */
  Queue& operator=(Queue&& other) = delete;
  void push(T const& value);
  bool isEmpty() { return p_stop == p_start; };
  T pop();

 private:
  size_t default_buffer_size;
  size_t buff_size;
  T* buffer;
  size_t p_start;
  size_t p_stop;
};

/* Конструктор */
template <typename T>
Queue<T>::Queue() {
  default_buffer_size = 4;
  buff_size = default_buffer_size;
  buffer = new T[buff_size];
  p_start = 0;
  p_stop = 0;
};

/* Деструктор */
template <typename T>
Queue<T>::~Queue() {
  delete[] buffer;
};

template <typename T>
void Queue<T>::push(T const& value) {
  buffer[p_stop] = value;
  p_stop = (p_stop + 1) % buff_size;
  if (p_start == p_stop) {
    T* new_buffer = new T[buff_size << 1];
    memcpy(new_buffer, buffer + p_start, (buff_size - p_start) * sizeof(T));
    memcpy(new_buffer + buff_size - p_start, buffer, p_stop * sizeof(T));
    delete[] buffer;
    p_start = 0;
    p_stop = buff_size;
    buff_size <<= 1;
    buffer = new_buffer;
  }
};

template <typename T>
T Queue<T>::pop() {
  assert(p_stop != p_start);
  T res = buffer[p_start];
  p_start = (p_start + 1) % buff_size;
  return res;
};

/* Main body */
int main(int argc, char* argv[]) {
  size_t number_of_commands;
  std::cin >> number_of_commands;

  bool correct_execution = true;
  Queue<int> myQueue;
  for (size_t i = 0; i < number_of_commands; ++i) {
    int command, arg;
    std::cin >> command >> arg;
    switch (command) {
      case 2:
        if ((not myQueue.isEmpty()) and (myQueue.pop() != arg)) {
          correct_execution = false;
        }
        break;
      case 3:
        myQueue.push(arg);
        break;
      default:
        correct_execution = false;
        break;
    }
  }
  std::cout << ((correct_execution) ? "YES" : "NO") << std::endl;
  return 0;
}
