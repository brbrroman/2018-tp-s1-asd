/*
 * Language: C++14
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 5.1
 * Во всех задачах данного раздела необходимо реализовать и использовать
 * сортировку слиянием. Общее время работы алгоритма O(n log n).
 *
 *                                 Реклама
 * В супермаркете решили оптимизировать показ рекламы. Известно расписание
 * прихода и ухода покупателей (два целых числа). Каждому покупателю необходимо
 * показать минимум 2 рекламы. Рекламу можно транслировать только в
 * целочисленные моменты времени. Покупатель может видеть рекламу от момента
 * прихода до момента ухода из магазина.
 *
 * В каждый момент времени может показываться только одна реклама. Считается,
 * что реклама показывается мгновенно. Если реклама показывается в момент ухода
 * или прихода, то считается, что посетитель успел её посмотреть. Требуется
 * определить минимальное число показов рекламы.
 *
 * Пример ввода       | Пример вывода
 * ===================|===============
 *   5                |   5
 *   1  10            |
 *  10  12            |
 *   1  10            |
 *   1  10            |
 *  23  24            |
 * ___________________|_______________
 */

#include <iostream>

/* Структура визита */
struct Visit {
  size_t start_at;
  size_t end_at;
  // bool operator<(const Visit& other);
};

class Compare {
 public:
  bool operator()(const Visit& a, const Visit& b) {
    return ((a.end_at < b.end_at) ||
            ((a.end_at == b.end_at) && (a.start_at > b.start_at)));
  }
};

template <typename T, typename Comparator>
void merge(T* const A, size_t start, const size_t mid, const size_t end,
           Comparator cmp) {
  size_t p = start, q = mid + 1;

  T Arr[end - start + 1];
  size_t k = 0;

  for (size_t i = start; i <= end; i++) {
    if (p > mid)
      Arr[k++] = A[q++];
    else if (q > end)
      Arr[k++] = A[p++];
    else if (cmp(A[p], A[q]))
      Arr[k++] = A[p++];
    else
      Arr[k++] = A[q++];
  }
  for (size_t p = 0; p < k; p++) {
    A[start++] = Arr[p];
  }
}

template <typename T, typename Comparator>
void merge_sort(T* const A, const size_t start, const size_t end,
                Comparator cmp) {
  if (start < end) {
    size_t mid = (start + end) / 2;
    merge_sort(A, start, mid, cmp);
    merge_sort(A, mid + 1, end, cmp);
    merge(A, start, mid, end, cmp);
  }
}

template <typename T, typename Comparator>
size_t get_adv_count(T* const array, size_t const size, Comparator cmp) {
  merge_sort(array, 0, size, cmp);

  size_t adv_count = 0;
  size_t adv2 = 0;
  size_t adv1 = 0;
  T visit;
  size_t i = 0;
  if (size > 0) {
    visit = array[i];
    ++i;
    adv2 = visit.end_at;
    adv1 = adv2 - 1;
    adv_count = 2;
  }
  while (i < size) {
    visit = array[i];
    ++i;
    if (visit.start_at > adv1) {
      adv1 = adv2;
      ++adv_count;
      adv2 = visit.end_at;
      if (visit.start_at > adv1) {
        if (visit.start_at != visit.end_at) {
          ++adv_count;
          adv1 = adv2 - 1;
        }
      }
    }
  }
  return adv_count;
}

/* Main body */
int main(int argc, char* argv[]) {
  /* Ввод данных */
  size_t size;
  std::cin >> size;
  Visit visits_vector[size];
  for (size_t i = 0; i < size; ++i) {
    std::cin >> visits_vector[size - i - 1].start_at >>
        visits_vector[size - i - 1].end_at;
  }
  std::cout << get_adv_count(visits_vector, size, Compare()) << std::endl;
  return 0;
}
