/*
 * Language: C++17
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 7.2
 *
 * Дан массив неотрицательных целых 64-битных чисел. Количество чисел
 * не больше 1000000. Отсортировать массив методом поразрядной
 * сортировки LSD по байтам.
 *
 * Пример
 * Ввод 	Вывод
 * 3        4 7 1000000
 * 4 1000000 7
 */

#include <algorithm>
#include <cmath>  //for log2(max) optimization
//#include <cstddef>
#include <assert.h>
#include <iostream>

class Compare {
 public:
  bool operator()(const unsigned long long int& a,
                  const unsigned long long int& b) {
    return a < b;
  }
};

template <typename T, typename Comparator>
void count_sort(T* array, size_t const size, size_t const shift,
                Comparator cmp) {
  T output[size];
  size_t i, count[256] = {0};

  for (size_t i = 0; i < size; ++i) {
    count[(array[i] >> shift) % 256]++;
  };

  for (i = 1; i < 256; ++i) {
    count[i] += count[i - 1];
  };  // запись заканчивается на
  for (size_t i = size; 0 < i; --i) {
    output[--count[(array[i - 1] >> shift) % 256]] = array[i - 1];
  };
  for (size_t i = 0; i < size; ++i) {
    array[i] = output[i];
  };
}

//От меньших разрядов к большим
template <typename T, typename Comparator>
void radix_sort(T* array, size_t const size, Comparator cmp,
                size_t const max_byte = sizeof(T)) {
  if (0 < size) {
    for (size_t digit = 0; digit < max_byte; ++digit) {
      count_sort(array, size, (digit << 3), cmp);
    }
  }
}

template <typename T>
void printArray(T* const from_index, T* const to_index) {
  assert(from_index <= to_index);
  for (T* iter = from_index; iter < to_index; ++iter) {
    std::cout << *iter << ' ';
  }
  std::cout << std::endl;
}

/* Main body */
int main(int argc, char* argv[]) {
  size_t size;
  std::cin >> size;
  unsigned long long int max = 0;
  unsigned long long int* array = new unsigned long long int[size];
  for (size_t i = 0; i < size; ++i) {
    std::cin >> array[i];
    if (array[i] > max) {
      max = array[i];
    }
  }
  radix_sort(array, size, Compare(), ceil(std::log2(max) / 8));
  printArray(array, array + size);
  delete[] array;
  return 0;
}
