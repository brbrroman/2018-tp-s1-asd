/*
 * Language: C++14
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 4.3
 * Решение всех задач данного раздела предполагает использование кучи,
 * реализованной в в иде класса.
 *
 *                                  Тупики
 * На вокзале есть некоторое количество тупиков, куда прибывают электрички. Этот
 * вокзал является их конечной станцией. Дано расписание движения электричек, в
 * котором для каждой электрички указано время ее прибытия, а также время
 * отправления в следующий рейс. Электрички в расписании упорядочены по времени
 * прибытия. Когда электричка прибывает, ее ставят в свободный тупик с
 * минимальным номером. При этом если электричка из какого-то тупика отправилась
 * в момент времени X, то электричку, которая прибывает в момент времени X, в
 * этот тупик ставить нельзя, а электричку, прибывающую в момент X+1 — можно.
 *
 * В данный момент на вокзале достаточное количество тупиков для работы по
 * расписанию.
 *
 * Напишите программу, которая по данному расписанию определяет, какое
 * минимальное количество тупиков требуется для работы вокзала.
 *
 * Формат входных данных. Вначале вводится n - количество электричек в
 * расписании. Затем вводится n строк для каждой электрички, в строке - время
 * прибытия и время отправления. Время - натуральное число от 0 до 10^9.
 * Строки в расписании упорядочены по времени прибытия.
 *
 * Формат выходных данных. Натуральное число - минимальное количеством тупиков.
 * Максимальное время: 50мс, память: 5Мб.
 *
 * Пример ввода       | Пример вывода
 * ===================|===============
 *   1                |   1
 *  10  20            |
 * -------------------+---------------
 *   2                |   2
 *  10  20            |
 *  20  25            |
 * -------------------+---------------
 *   3                |   2
 *  10  20            |
 *  20  25            |
 *  21  31            |
 * ___________________|_______________
 */

#include <cstring>
#include <iostream>

struct Train {
  size_t departure_time;
  size_t arrival_time;
  bool operator<(const Train& right) {
    return (this->departure_time < right.departure_time);
  }
};

template <typename T>
class CHeap {
 public:
  CHeap();
  ~CHeap();
  void push(const T value);
  T pop();
  T top();
  size_t size();

 private:
  size_t parent(const size_t index) { return (index - 1) / 2; };
  size_t leftChild(const size_t index) { return (index + 1) * 2 - 1; };
  size_t rightChild(const size_t index) { return (index + 1) * 2; };
  size_t buffer_size;
  T* buffer;
  size_t heap_size;
  void siftUp(size_t index);
  void siftDown(size_t index);
};

/* Конструктор */
template <typename T>
CHeap<T>::CHeap() {
  buffer_size = 2;
  buffer = new T[buffer_size];
  heap_size = 0;
};

/* Деструктор */
template <typename T>
CHeap<T>::~CHeap() {
  delete[] buffer;
};

/* Узнать корень */
template <typename T>
T CHeap<T>::top() {
  return buffer[0];
}

/* Вернуть количество элементов */
template <typename T>
size_t CHeap<T>::size() {
  return heap_size;
};

/* Добавить элемент в кучу */
template <typename T>
void CHeap<T>::push(const T value) {
  buffer[heap_size] = value;
  siftUp(heap_size);
  heap_size += 1;
  if (heap_size == buffer_size) {
    T* new_buffer = new T[buffer_size * 2];
    std::memcpy(new_buffer, buffer, buffer_size * sizeof(T));
    delete[] buffer;
    buffer_size <<= 1;
    buffer = new_buffer;
  }
};

/* Забрать корень */
template <typename T>
T CHeap<T>::pop() {
  T result = buffer[0];
  if (0 < heap_size) {
    buffer[0] = buffer[heap_size - 1];
    siftDown(0);
    heap_size--;
  }
  return result;
};

/* Просеять вверх */
template <typename T>
void CHeap<T>::siftUp(size_t index) {
  while ((0 < index) and (buffer[index] < buffer[parent(index)])) {
    std::swap(buffer[parent(index)], buffer[index]);
    index = parent(index);
  }
};

/* Просеять вниз */
template <typename T>
void CHeap<T>::siftDown(size_t index) {
  size_t maxIndex = index;
  size_t leftIndex = leftChild(index);
  if ((leftIndex < heap_size) and (buffer[leftIndex] < buffer[maxIndex])) {
    maxIndex = leftIndex;
  }
  size_t rightIndex = rightChild(index);
  if ((rightIndex < heap_size) and (buffer[rightIndex] < buffer[maxIndex])) {
    maxIndex = rightIndex;
  }
  if (index != maxIndex) {
    std::swap(buffer[index], buffer[maxIndex]);
    siftDown(maxIndex);
  }
};

/* Main body */
int main(int argc, char* argv[]) {
  size_t size;
  std::cin >> size;
  size_t max = 0;
  CHeap<Train> myHeap;
  for (size_t i = 0; i < size; ++i) {
    Train tmpTrain;
    std::cin >> tmpTrain.arrival_time >> tmpTrain.departure_time;
    myHeap.push(tmpTrain);
    while ((myHeap.size() > 0) and
           (myHeap.top().departure_time < tmpTrain.arrival_time)) {
      myHeap.pop();
    }
    if (myHeap.size() > max) {
      max = myHeap.size();
    }
  }
  std::cout << max << std::endl;
  return 0;
}
