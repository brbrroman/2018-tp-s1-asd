/*
 * Language: C++14
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 6.2
 * Даны неотрицательные целые числа n,k и массив целых чисел из [0..10^9]
 * размера n. Требуется найти k-ю порядковую статистику. т.е. напечатать число,
 * которое бы стояло на позиции с индексом k (0..n-1) в отсортированном массиве.
 * Напишите нерекурсивный алгоритм.
 *
 * Требования к дополнительной памяти: O(n). Требуемое среднее время работы:
 * O(n).
 *
 * Функцию get_partition следует реализовывать методом прохода двумя итераторами
 * в одном направлении. Описание для случая прохода от начала массива к концу:
 * - Выбирается опорный элемент. Опорный элемент меняется с последним элементом
 *   массива.
 * - Во время работы get_partition в начале массива содержатся элементы, не
 *   бОльше опорного. Затем располагаются элементы, строго бОльшие опорного. В
 * конце массива лежат нерассмотренные элементы. Последним элементом лежит
 * опорный.
 * - Итератор (индекс) i указывает на начало группы элементов, строго бОльших
 *   опорного.
 * - Итератор j больше i, итератор j указывает на первый нерассмотренный
 *   элемент.
 * - Шаг алгоритма. Рассматривается элемент, на который указывает j. Если он
 *   больше опорного, то сдвигаем j.
 *   Если он не больше опорного, то меняем a[i] и a[j] местами, сдвигаем i и
 *   сдвигаем j.
 * - В конце работы алгоритма меняем опорный и элемент, на который указывает
 *   итератор i.
 *
 * Реализуйте стратегию выбора опорного элемента “медиана трёх”. Функцию
 * get_partition реализуйте методом прохода двумя итераторами от конца массива к
 * началу.
 *
 * Пример ввода                             | Пример вывода
 * =========================================|===============
 *  10   4                                  |   5
 *   1   2   3   4   5   6   7   8   9  10  |
 * -----------------------------------------+---------------
 *  10   0                                  |   1
 *   3   6   5   7   2   9   8  10   4   1  |
 * -----------------------------------------+---------------
 *  10   9                                  |   1
 *   0   0   0   0   0   0   0   0   0   1  |
 * _________________________________________|_______________
 */

#include <iostream>

class Compare {
 public:
  bool operator()(const int &a, const int &b) { return a < b; }
};

template <typename T>
size_t get_median(T const &arr, size_t const size) {
  size_t mid = size / 2;
  if (arr[0] < arr[mid]) {
    if (arr[size - 1] < arr[0]) {
      return 0;
    } else if (arr[size - 1] < arr[mid]) {
      return size - 1;
    }
    return mid;
  } else {
    if (arr[size - 1] < arr[mid]) {
      return mid;
    } else if (arr[size - 1] < arr[0]) {
      return size - 1;
    }
    return 0;
  }
};

template <typename T, typename Comparator>
size_t get_partition(T &arr, size_t const from_index, size_t const size,
                     Comparator cmp) {
  size_t median = from_index + get_median(arr + from_index, size - from_index);
  size_t pivot = from_index;
  std::swap(arr[median], arr[pivot]);
  size_t i = size - 1;
  for (size_t j = size - 1; pivot < j; --j) {
    if (cmp(arr[pivot], arr[j])) {
      std::swap(arr[j], arr[i--]);
    }
  }
  std::swap(arr[pivot], arr[i]);
  return i;
};

template <typename T, typename Comparator>
int get_elem(T &arr, size_t const pos, size_t const size, Comparator cmp) {
  size_t left = 0, right = size - 1;
  size_t mid = get_partition(arr, left, right + 1, cmp);
  while (mid != pos) {
    if (pos < mid) {
      right = mid - 1;
    } else {
      left = mid + 1;
    }
    mid = get_partition(arr, left, right + 1, cmp);
  }
  return arr[mid];
};

int main() {
  size_t size, pos;
  std::cin >> size >> pos;
  int *array = new int[size];

  for (size_t i = 0; i < size; ++i) {
    std::cin >> array[i];
  }

  std::cout << get_elem(array, pos, size, Compare()) << std::endl;
  delete[] array;
  return 0;
}
