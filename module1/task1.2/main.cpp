/*
 * Language: C++14
 * Author:   Vekshin Roman
 */

/*
 *                                Задача 1.2
 *
 * Вычислить площадь выпуклого n-угольника, заданного координатами своих
 * вершин. Вначале вводится количество вершин, затем последовательно
 * целочисленные координаты всех вершин в порядке обхода по часовой
 * стрелке.
 *
 * n < 1000, координаты < 10000.
 *
 * Указание. Для вычисления площади n-угольника можно посчитать сумму
 * ориентированных площадей трапеций под каждой стороной многоугольника.
 * Пример
 * Ввод   Вывод
 * 3    1.5
 * 1 0
 * 0 1
 * 2 2
 *
 */

#include <cmath>
#include <iostream>
double square(const int* const x, const int* const y, size_t const size) {
  double square = 0;
  for (size_t i = 0; i < size; ++i) {
    square += x[i] * y[(i + 1) % size] - x[(i + 1) % size] * y[i];
  }
  return fabs(square / 2);
}

/* Main body */
int main() {
  size_t size = 0;
  std::cin >> size;
  int* array_x = new int[size];
  int* array_y = new int[size];

  for (size_t i = 0; i < size; ++i) {
    std::cin >> array_x[i] >> array_y[i];
  }

  std::cout << square(array_x, array_y, size) << std::endl;

  delete[] array_x;
  array_x = nullptr;
  delete[] array_y;
  array_y = nullptr;

  return 0;
}
