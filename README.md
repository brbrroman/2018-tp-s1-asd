# tp-s1-asd
Технопарк Mail.Ru / 1-ый семестр / Алгоритмы и структуры данных

Решение варианта №28:

| Модуль 1 | Модуль 2 | Модуль 3 |
|-|-|-|
| [Задача 1.2](module1/task1.2/main.cpp) | [Задача 1.2](module2/task1.2/main.cpp) | [Задача 1](module1/task1/main.cpp) |
| [Задача 2.4](module1/task2.4/main.cpp) | [Задача 2.2](module2/task2.2/main.cpp) | [Задача 2](module1/task2/main.cpp) |
| [Задача 3.1](module1/task3.1/main.cpp) | [Задача 3.2](module2/task3.2/main.cpp) | [Задача 3](module1/task3/main.cpp) |
| [Задача 4.3](module1/task4.3/main.cpp) | [Задача 4.2](module2/task4.2/main.cpp) | [Задача 4](module1/task4/main.cpp) |
| [Задача 5.1](module1/task5.1/main.cpp) | [Задача 5](module2/task5/main.cpp) | [Задача 5.2](module1/task5.2/main.cpp) |
| [Задача 6.1](module1/task6.1/main.cpp) |
| [Задача 7.2](module1/task7.2/main.cpp) |

Полезные ссылки:
- [Задания модуля 1](https://docs.google.com/document/d/17PNVKTkdo8hiittN7I-zpOeP3u5s3ss7WTNqYUhsMMw)
- [Задания модуля 2](https://drive.google.com/open?id=1EiQ9tXdDgINKmfJ5a1Lo-hl5CegYDlE-MAvhyhs50qo)
- [Задания модуля 3](https://drive.google.com/open?id=1S0NTFfyRu3FZFSzb34ftJaQENwRZLEvgrNi--eYrmb0)
- [Тестирующая система модуля 1](https://contest.yandex.ru/contest/9367)
- [Тестирующая система модуля 2](https://contest.yandex.ru/contest/9749)
- [Тестирующая система модуля 3](https://contest.yandex.ru/contest/11054)
- [Ведомость](https://docs.google.com/spreadsheets/d/1aQGuiCJk_byu-mxZ-aHpyfzMXnTGDhqabku2FMxXRp4)
- [Правила кодирования](https://docs.google.com/document/d/1NxQAhxyhoRD59sURVDn1eUniqDWQKxqergrJgx-bRFo/)
